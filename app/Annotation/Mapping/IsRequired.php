<?php


namespace App\Annotation\Mapping;

use Doctrine\Common\Annotations\Annotation\Attribute;
use Doctrine\Common\Annotations\Annotation\Attributes;

/**
 * Required 注解命令
 * @Annotation
 * @Attributes({
 *     @Attribute("message",type="string")
 *     })
 * @package App\Annotation\Mapping
 */
class IsRequired
{
    /**
     * @var string
     */
    private $message='';

    /**
     * IsRequired constructor.
     */
    public function __construct(array $values)
    {
        if (isset($values['value'])) {
            $this->message = $values['value'];
        }
        if (isset($values['message'])) {
            $this->message = $values['message'];
        }
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

}
