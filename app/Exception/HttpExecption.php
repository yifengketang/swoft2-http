<?php


namespace App\Exception;


use Throwable;

class HttpExecption extends \Exception
{
    private $errorCode;
    private $data;

    public function __construct($message = "", $errorCode=1,$data=[],$code = 200, Throwable $previous = null)
    {
        $this->errorCode = $errorCode;
        $this->data = $data;
        parent::__construct($message, $code, $previous);
    }

    /**
     * 获取错误码
     * @return int
     */
    public function getErrorCode(){
        return $this->errorCode;
    }
    /**
     * 获取数据
     * @return int
     */
    public function getData(){
        return $this->data;
    }
}
