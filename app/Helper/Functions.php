<?php
function return_msg($code=0,$msg="操作成功",$data=[]){
    return [
        'code'=>$code,
        'msg'=>$msg,
        'data'=>$data
    ];
}

/**
 * 通过Key来获取session,返回的值是字符串
 * @param $key
 * @return string
 */
function get_session($key):string
{
   return \Swoft\Http\Session\HttpSession::current()->get($key);
}

/**
 * 通过Key获取配置信息
 * @param $key
 * @return string
 */
function get_config($key){
    $config = bean(\App\Model\Logic\ConfigLogic::class)->getData();
    return $config[$key]??"";
}
