<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://swoft.org/docs
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Http\Controller\Admin;

use App\Exception\HttpExecption;
use App\Http\Middleware\CheckLoginMiddleware;
use App\Model\Logic\ConfigLogic;
use Swoft\Bean\Annotation\Mapping\Inject;
use Swoft\Http\Message\Request;
use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\Middleware;
use Swoft\Http\Server\Annotation\Mapping\Middlewares;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;

/**
 * Class IndexController
 *
 * @Controller(prefix="/admin")
 * @Middlewares({
 *     @Middleware(CheckLoginMiddleware::class)
 *     })
 * @package App\Http\Controller
 */
class ConfigController{
    //注入逻辑层
    /**
     * @Inject()
     * @var ConfigLogic
     */
    private $configlogic;
    //表单
    /**
     * @RequestMapping("config")
     * @return \Swoft\Http\Message\Response
     * @throws \Throwable
     */
    public function index(){
        //获取配置信息
        $config = $this->configlogic->getData();
        return view("admin/config/config",["config"=>$config]);
    }

    /**
     * @RequestMapping("saveconfig")
     * @param Request $request
     * @throws \App\Exception\HttpExecption
     */
    public function saveConfig(Request $request){
        if($request->isAjax()){
            //获取数据
            $data = $request->post();
            //提交到逻辑层
            $this->configlogic->saveData($data);
        }
        throw new HttpExecption("请求异常");
    }
}
