<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://swoft.org/docs
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Http\Controller\Admin;

use App\Exception\HttpExecption;
use App\Model\Logic\CourseLogic;
use Swoft\Bean\Annotation\Mapping\Inject;
use Swoft\Http\Message\Request;
use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\Middleware;
use Swoft\Http\Server\Annotation\Mapping\Middlewares;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;
use App\Http\Middleware\CheckLoginMiddleware;
use Swoft\Http\Session\HttpSession;

/**
 * Class IndexController
 *
 * @Controller(prefix="/admin")
 * @Middlewares({
 *     @Middleware(CheckLoginMiddleware::class)
 *     })
 * @package App\Http\Controller
 */
class CourseController{
    /**
     * @Inject()
     * @var CourseLogic
     */
    private $courselogic;

    /**
     * @RequestMapping("course/list")
     * @return string
     */
    public function index(){
        return view("admin/course/list");
    }

    /**
     * @RequestMapping("course/create")
     * @return string
     * @throws \Throwable
     */
    public function create(){
        return view("admin/course/create");
    }
    /**
     * @RequestMapping("course/edit/{id}")
     * @return string
     * @throws \Throwable
     */
    public function edit(int $id){
        //获取课程信息
        $course = $this->courselogic->getCourseFromId($id);
        $data = [
            "course"=>$course
        ];
        return view("admin/course/edit",$data);
    }
    /**
     * @RequestMapping("course/save")
     */
    public function save(Request $request){
        //获取数据
        $data = $request->post();
        //提交到logic
        $this->courselogic->addData($data);
    }
    /**
     * 接收编辑数据并提交到逻辑层处理
     * @RequestMapping("course/update/{id}")
     */
    public function update(Request $request,int $id){
        //获取数据
        $data = $request->post();
        $datas = [
            "id"=>$id,
            "data"=>$data
        ];
        //提交到logic
        $this->courselogic->updateData($datas);
    }
    /**
     * @RequestMapping("course/getdata")
     */
    public function getData(Request $request){
        $data = $request->get();
        $data = $this->courselogic->getDatas($data);
        return context()->getResponse()->withData($data);
    }

    /**
     * 删除接口
     * @RequestMapping("course/del")
     */
    public function del(Request $request){
        $ids = $request->post("ids");
        $this->courselogic->del($ids);
    }

    /**
     * 设置程是否推荐
     * @RequestMapping("course/setshow")
     */
    public function setisshow(Request $request){
        $data = $request->post();
        $this->courselogic->setisshow($data);
    }
}
