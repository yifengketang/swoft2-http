<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://swoft.org/docs
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Http\Controller\Admin;

use App\Exception\HttpExecption;
use App\Model\Logic\UploadLogic;
use Swoft\Bean\Annotation\Mapping\Inject;
use Swoft\Http\Message\Request;
use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\Middleware;
use Swoft\Http\Server\Annotation\Mapping\Middlewares;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;
use App\Http\Middleware\CheckLoginMiddleware;
use Swoft\Http\Session\HttpSession;

/**
 * Class IndexController
 *
 * @Controller(prefix="/admin")
 * @Middlewares({
 *     @Middleware(CheckLoginMiddleware::class)
 *     })
 * @package App\Http\Controller
 */
class IndexController{
    /**
     * this is a example action. access uri path: /index
     * @RequestMapping(route="/admin", method=RequestMethod::GET)
     * @return array
     */
    public function index()
    {
        return view("admin/index/index");
    }

    /**
     * @RequestMapping("welcome")
     * @return \Swoft\Http\Message\Response
     * @throws \Throwable
     */
    public function welcome(){
        return view("admin/index/welcome");
    }

    /**
     * 后台菜单接口
     * @RequestMapping("menu")
     * @return \Swoft\Http\Message\Response
     */
    public function getMenu(){
        $menu = config("menu");
        return context()->getResponse()->withData($menu);
    }

    /**
     * @Inject()
     * @var UploadLogic
     */
    private $uploadlogic;
    /**
     * @RequestMapping("upload")
     */
    public function upload(Request $request){
        $file = $request->getUploadedFiles();
        if($file){
            $this->uploadlogic->upload($file["file"]);
        }else{
            throw new HttpExecption("请选择上传文件");
        }

    }
}
