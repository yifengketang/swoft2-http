<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://swoft.org/docs
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Http\Controller\Admin;

use App\Exception\HttpExecption;
use App\Http\Middleware\CheckLoginMiddleware;
use Swoft\Bean\Annotation\Mapping\Inject;
use Swoft\Http\Message\Request;
use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\Middleware;
use Swoft\Http\Server\Annotation\Mapping\Middlewares;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;
use App\Model\Logic\ManagerLogic;
/**
 * Class IndexController
 *
 * @Controller(prefix="/admin")
 * @Middlewares({
 *     @Middleware(CheckLoginMiddleware::class)
 *     })
 * @package App\Http\Controller
 */
class ManagerController{
    /**
     * @Inject()
     * @var ManagerLogic
     */
    protected $managerlogic;
    /**
     * this is a example action. access uri path: /index
     * @RequestMapping(route="setpassword", method=RequestMethod::GET)
     * @return array
     * @throws \Throwable
     */
    public function setpassword()
    {
        return view("admin/manager/setpassword");
    }

    /**
     * @RequestMapping("dopassword")
     */
    public function dopassword(Request $request){
        if($request->isAjax()){
            //接收数据
            $data= $request->post();
            //提交逻辑层处理
            $this->managerlogic->updatePassword($data);
        }
        throw new HttpExecption("请求异常",1);
    }
}
