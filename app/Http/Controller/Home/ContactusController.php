<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://swoft.org/docs
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Http\Controller\Home;

use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;

/**
 * Class ContactusController
 *
 * @Controller()
 * @package App\Http\Controller
 */
class ContactusController{
    /**
     * this is a example action. access uri path: /contactus
     * @RequestMapping(route="/linkus", method=RequestMethod::GET)
     */
    public function index()
    {
        return \Swoft::getBean("view")->render("home/linkus");
    }
}
