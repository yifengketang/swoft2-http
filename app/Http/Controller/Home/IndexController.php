<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://swoft.org/docs
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Http\Controller\Home;

use App\Model\Logic\CourseLogic;
use Swoft\Bean\BeanFactory;
use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;

/**
 * Class IndexController
 *
 * @Controller()
 * @package App\Http\Controller
 */
class IndexController{
    /**
     * Get data list. access uri path: /index
     * @RequestMapping(route="/", method=RequestMethod::GET)
     * @return \Swoft\Http\Message\Response
     * @throws \Throwable
     */
    public function index()
    {
        //获取最新推荐课程
        $data = BeanFactory::getBean(CourseLogic::class)->getNewsCourse(8);
        return view("home/index",['list'=>$data]);
    }
}
