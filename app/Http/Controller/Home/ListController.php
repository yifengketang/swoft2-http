<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://swoft.org/docs
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Http\Controller\Home;

use App\Exception\HttpExecption;
use App\Model\Logic\CourseLogic;
use Swoft\Bean\BeanFactory;
use Swoft\Http\Message\Request;
use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;

/**
 * Class ListController
 *
 * @Controller()
 * @package App\Http\Controller
 */
class ListController{
    /**
     * this is a example action. access uri path: /list
     * @RequestMapping(route="/list", method=RequestMethod::GET)
     * @return \Swoft\Http\Message\Response
     * @throws \Throwable
     */
    public function index()
    {
        return view("home/list");
    }

    /**
     * @RequestMapping("/api/getcourses")
     * @param Request $request
     * @return \Swoft\Http\Message\Response
     * @throws HttpExecption
     */
    public function getdata(Request $request){
        if(!$request->isAjax()){
            throw new HttpExecption("请求异常");
        }
        $parms = $request->get();
        $data = BeanFactory::getBean(CourseLogic::class)->getDatas($parms);
        return context()->getResponse()->withData($data);
    }
}
