<?php


namespace App\Model\Dao;

use App\Model\Entity\Config;
use Swoft\Bean\Annotation\Mapping\Bean;

/**
 * 管理员操作
 * @Bean()
 * @package App\Model\Dao
 */
class ConfigDao
{
    /**
     * 获取配置信息
     * @return object|\Swoft\Db\Eloquent\Builder|\Swoft\Db\Eloquent\Model|null
     * @throws \Swoft\Db\Exception\DbException
     */
    public function getConfig()
    {
        $config = Config::first();
        return $config;
    }

    /**
     * @param $data
     * @return bool
     */
    public function updateCreate($data){
        //写入数据
        $result = Config::updateOrCreate(['id'=>1],$data);
        if($result){
            return true;
        }
        return false;
    }
}
