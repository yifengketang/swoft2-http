<?php


namespace App\Model\Dao;

use App\Model\Entity\Course;
use Swoft\Bean\Annotation\Mapping\Bean;

/**
 * 管理员操作
 * @Bean()
 * @package App\Model\Dao
 */
class CourseDao
{
    /**
     * @param $data
     * @return bool
     */
    public function add($data){
        //写入数据
        $result = Course::insert($data);
        if($result){
            return true;
        }
        return false;
    }

    /**
     * 更新入库
     * @param $data
     * @return bool
     * @throws \ReflectionException
     * @throws \Swoft\Bean\Exception\ContainerException
     * @throws \Swoft\Db\Exception\DbException
     */
    public function updateData($data){
        //写入数据
        $result = Course::where("id",$data["id"])->update($data["data"]);
        if($result){
            return true;
        }
        return false;
    }

    /**
     * 实现数据的分页与查询
     * @param $parms
     * @return array
     * @throws \Swoft\Db\Exception\DbException
     */
    public function getDatas($parms){
        $count = Course::count();
        $data = Course::where($parms['where'])->forPage(intval($parms["page"]), intval($parms["limit"]))->get();
        return ['count'=>$count,"data"=>$data];
    }

    /**
     * 数据的删除
     * @param $ids
     * @return bool
     * @throws \ReflectionException
     * @throws \Swoft\Bean\Exception\ContainerException
     * @throws \Swoft\Db\Exception\DbException
     */
    public function del($ids){
        if(Course::whereIn("id",$ids)->delete()){
            return true;
        }
        return false;
    }

    /**
     * 设置课程的推荐状态
     * @param $data
     * @return bool
     * @throws \ReflectionException
     * @throws \Swoft\Bean\Exception\ContainerException
     * @throws \Swoft\Db\Exception\DbException
     */
    public function setisshow($data){
        if(Course::where("id",$data["id"])->update(['isshow'=>$data['isshow']])){
            return true;
        }
        return false;
    }
    //通过ID获取课程信息
    public function getCourseFromId($id){
         $course = Course::find($id);
        if($course){
            return $course;
        }
        return false;
    }
    //获取最新推荐课程
    public function getNewsCourse($num){
        $data = Course::where("isshow",1)->orderByDesc("created_at")->take($num)->get();
        return $data;
    }
}
