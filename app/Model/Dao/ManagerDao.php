<?php


namespace App\Model\Dao;

use App\Model\Entity\Manager;
use Swoft\Bean\Annotation\Mapping\Bean;
use Swoft\Http\Session\HttpSession;

/**
 * 管理员操作
 * @Bean()
 * @package App\Model\Dao
 */
class ManagerDao
{
    /**
     * 通过用户名获取用户信息
     * @param $username
     * @return object|\Swoft\Db\Eloquent\Builder|\Swoft\Db\Eloquent\Model|null
     * @throws \Swoft\Db\Exception\DbException
     */
    public function getUserFromUsername($username)
    {
        $user = Manager::where('username',$username)->select("id","username","password")->first();
        return $user;
    }

    /**
     * 密码更新
     * @param $password
     * @return bool
     * @throws \ReflectionException
     * @throws \Swoft\Bean\Exception\ContainerException
     * @throws \Swoft\Db\Exception\DbException
     */
    public function updatePassword($password){
        $data = [
            'password'=>password_hash($password,PASSWORD_DEFAULT)
        ];
       $result = Manager::where('id',HttpSession::current()->get("userid"))->update($data);
       if($result){
           return true;
       }
       return false;
    }
}
