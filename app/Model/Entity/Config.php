<?php declare(strict_types=1);


namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 站点配置
 * Class Config
 *
 * @since 2.0
 *
 * @Entity(table="config")
 */
class Config extends Model
{
    /**
     * 
     * @Id()
     * @Column()
     *
     * @var int
     */
    private $id;

    /**
     * 站点名称
     *
     * @Column()
     *
     * @var string
     */
    private $webtitle;

    /**
     * 关键字
     *
     * @Column()
     *
     * @var string
     */
    private $webkey;

    /**
     * 描述
     *
     * @Column()
     *
     * @var string
     */
    private $webdesc;

    /**
     * email
     *
     * @Column()
     *
     * @var string
     */
    private $email;

    /**
     * 
     *
     * @Column()
     *
     * @var string
     */
    private $weburl;

    /**
     * qq
     *
     * @Column()
     *
     * @var string
     */
    private $qq;

    /**
     * 版权信息
     *
     * @Column()
     *
     * @var string
     */
    private $copyright;


    /**
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $webtitle
     *
     * @return self
     */
    public function setWebtitle(string $webtitle): self
    {
        $this->webtitle = $webtitle;

        return $this;
    }

    /**
     * @param string $webkey
     *
     * @return self
     */
    public function setWebkey(string $webkey): self
    {
        $this->webkey = $webkey;

        return $this;
    }

    /**
     * @param string $webdesc
     *
     * @return self
     */
    public function setWebdesc(string $webdesc): self
    {
        $this->webdesc = $webdesc;

        return $this;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $weburl
     *
     * @return self
     */
    public function setWeburl(string $weburl): self
    {
        $this->weburl = $weburl;

        return $this;
    }

    /**
     * @param string $qq
     *
     * @return self
     */
    public function setQq(string $qq): self
    {
        $this->qq = $qq;

        return $this;
    }

    /**
     * @param string $copyright
     *
     * @return self
     */
    public function setCopyright(string $copyright): self
    {
        $this->copyright = $copyright;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getWebtitle(): ?string
    {
        return $this->webtitle;
    }

    /**
     * @return string
     */
    public function getWebkey(): ?string
    {
        return $this->webkey;
    }

    /**
     * @return string
     */
    public function getWebdesc(): ?string
    {
        return $this->webdesc;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getWeburl(): ?string
    {
        return $this->weburl;
    }

    /**
     * @return string
     */
    public function getQq(): ?string
    {
        return $this->qq;
    }

    /**
     * @return string
     */
    public function getCopyright(): ?string
    {
        return $this->copyright;
    }

}
