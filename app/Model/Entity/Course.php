<?php declare(strict_types=1);


namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 课程表
 * Class Course
 *
 * @since 2.0
 *
 * @Entity(table="course")
 */
class Course extends Model
{
    /**
     * 
     * @Id()
     * @Column()
     *
     * @var int
     */
    private $id;

    /**
     * 
     *
     * @Column()
     *
     * @var string
     */
    private $title;

    /**
     * 
     *
     * @Column()
     *
     * @var string
     */
    private $keyword;

    /**
     * 
     *
     * @Column()
     *
     * @var string
     */
    private $desc;

    /**
     * 
     *
     * @Column()
     *
     * @var string
     */
    private $digest;

    /**
     * 
     *
     * @Column()
     *
     * @var string
     */
    private $pic;

    /**
     * 
     *
     * @Column()
     *
     * @var string|null
     */
    private $content;

    /**
     * 
     *
     * @Column()
     *
     * @var int
     */
    private $isshow;

    /**
     * 
     *
     * @Column(name="created_at", prop="createdAt")
     *
     * @var int
     */
    private $createdAt;

    /**
     * 
     *
     * @Column(name="updated_at", prop="updatedAt")
     *
     * @var int
     */
    private $updatedAt;


    /**
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param string $keyword
     *
     * @return self
     */
    public function setKeyword(string $keyword): self
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * @param string $desc
     *
     * @return self
     */
    public function setDesc(string $desc): self
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * @param string $digest
     *
     * @return self
     */
    public function setDigest(string $digest): self
    {
        $this->digest = $digest;

        return $this;
    }

    /**
     * @param string $pic
     *
     * @return self
     */
    public function setPic(string $pic): self
    {
        $this->pic = $pic;

        return $this;
    }

    /**
     * @param string|null $content
     *
     * @return self
     */
    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @param int $isshow
     *
     * @return self
     */
    public function setIsshow(int $isshow): self
    {
        $this->isshow = $isshow;

        return $this;
    }

    /**
     * @param int $createdAt
     *
     * @return self
     */
    public function setCreatedAt(int $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @param int $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(int $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    /**
     * @return string
     */
    public function getDesc(): ?string
    {
        return $this->desc;
    }

    /**
     * @return string
     */
    public function getDigest(): ?string
    {
        return $this->digest;
    }

    /**
     * @return string
     */
    public function getPic(): ?string
    {
        return $this->pic;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getIsshow(): ?int
    {
        return $this->isshow;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): ?int
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): ?int
    {
        return $this->updatedAt;
    }

}
