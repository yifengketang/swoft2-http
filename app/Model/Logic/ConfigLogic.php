<?php


namespace App\Model\Logic;


use App\Exception\HttpExecption;
use App\Model\Dao\ConfigDao;
use Swoft\Bean\Annotation\Mapping\Bean;
use Swoft\Bean\Annotation\Mapping\Inject;

/**
 * 管理员登录
 * @Bean()
 * @package App\Model\Logic
 */
class ConfigLogic
{
    /**
     * @Inject()
     * @var ConfigDao
     */
    private $configdao;

    public function saveData($data){
        //数据验证
        validate($data,"ConfigValidator");
        //更新到数据表
       if($this->configdao->updateCreate($data)){
           throw new HttpExecption("更新成功",0);
       }
        throw new HttpExecption("更新失败",1);
    }

    /**
     * 获取配置信息
     * @return object|\Swoft\Db\Eloquent\Builder|\Swoft\Db\Eloquent\Model|null
     * @throws \Swoft\Db\Exception\DbException
     */
    public function getData(){
        return $this->configdao->getConfig();
    }
}
