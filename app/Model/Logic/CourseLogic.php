<?php


namespace App\Model\Logic;


use App\Exception\HttpExecption;
use App\Model\Dao\CourseDao;
use Swoft\Bean\Annotation\Mapping\Bean;
use Swoft\Bean\Annotation\Mapping\Inject;

/**
 * 管理员登录
 * @Bean()
 * @package App\Model\Logic
 */
class CourseLogic
{
    /**
     * @Inject()
     * @var CourseDao
     */
    private $coursedao;

    public function addData($data){
        //数据验证
        validate($data,"CourseValidator");
        //更新到数据表
       if($this->coursedao->add($data)){
           throw new HttpExecption("添加成功",0);
       }
        throw new HttpExecption("添加失败",1);
    }

    /**
     * 更新入库
     * @param $data
     * @throws HttpExecption
     * @throws \ReflectionException
     * @throws \Swoft\Bean\Exception\ContainerException
     * @throws \Swoft\Db\Exception\DbException
     * @throws \Swoft\Validator\Exception\ValidatorException
     */
    public function updateData($data){
        //数据验证
        validate($data,"CourseValidator");
        //更新到数据表
        if($this->coursedao->updateData($data)){
            throw new HttpExecption("更新成功",0);
        }
        throw new HttpExecption("更新失败",1);
    }

    /**
     * 数据的查询与分页
     * @param $parms
     * @return array
     * @throws \Swoft\Db\Exception\DbException
     */
    public function getDatas($parms){
        $where= [];
        if(isset($parms['title'])){
            $where= [
                ["title",'like',"%".$parms['title']."%"]
            ];
        }
        $datas = [
            'page'=>$parms["page"]??1,
            'limit'=>$parms["limit"]??15,
            'where'=>$where
        ];
        $data =  $this->coursedao->getDatas($datas);
        $datas = [
            "code"=>0,
            "msg"=>"ok",
            "count"=>$data['count'],
            "data"=>$data['data']
        ];
        return $datas;
    }

    /**
     * 删除
     * @param $ids
     * @throws HttpExecption
     * @throws \ReflectionException
     * @throws \Swoft\Bean\Exception\ContainerException
     * @throws \Swoft\Db\Exception\DbException
     */
    public function del($ids){
        $ids = explode(",",$ids);
        if($this->coursedao->del($ids)){
            throw new HttpExecption("删除成功",0);
        }else{
            throw new HttpExecption("添加失败",1);
        }
    }

    /**
     * 设置状态
     * @param $data
     * @throws HttpExecption
     * @throws \ReflectionException
     * @throws \Swoft\Bean\Exception\ContainerException
     * @throws \Swoft\Db\Exception\DbException
     */
    public function setisshow($data){
        if($this->coursedao->setisshow($data)){
            throw new HttpExecption("操作成功",0);
        }else{
            throw new HttpExecption("操作失败",1);
        }
    }

    /**
     * 通过ID获取课程信息
     * @param $id
     * @return mixed
     * @throws HttpExecption
     */
    public function getCourseFromId($id){
        $result = $this->coursedao->getCourseFromId($id);
        if($result!==false){
            return $result;
        }
        throw new HttpExecption("课程不存在");
    }

    /**
     * 获取最新推荐课程
     * @param int $num
     * @return \Swoft\Db\Eloquent\Collection
     */
    public function getNewsCourse($num=4){
        return $this->coursedao->getNewsCourse($num);
    }
}
