<?php


namespace App\Model\Logic;


use App\Exception\HttpExecption;
use App\Model\Dao\ManagerDao;
use App\Model\Entity\Manager;
use App\Validator\LoginValidator;
use App\Validator\User\UserNameValidator;
use Swoft\Bean\Annotation\Mapping\Bean;
use Swoft\Bean\Annotation\Mapping\Inject;
use Swoft\Http\Session\HttpSession;

/**
 * 管理员登录
 * @Bean()
 * @package App\Model\Logic
 */
class ManagerLogic
{
    /**
     * @Inject()
     * @var ManagerDao
     */
    private $managerdao;

    public function updatePassword($data){
        //数据验证
        validate($data,"LoginValidator",["oldpassword","password","repassword"]);
        //判断旧密码是否正确
        $user = $this->managerdao->getUserFromUsername(HttpSession::current()->get("username"));
        if(!password_verify($data["oldpassword"],$user["password"])){
            throw new HttpExecption("旧密码输入不正确",1);
        }
        //密码的更新
       if($this->managerdao->updatePassword($data["password"])){
           throw new HttpExecption("密码修改成功",0);
       }else{
           throw new HttpExecption("密码修改失败",1);
       }
    }
}
