<?php


namespace App\Validator;

use App\Annotation\Mapping\IsRequired;
use Swoft\Validator\Annotation\Mapping\Alpha;
use Swoft\Validator\Annotation\Mapping\Confirm;
use Swoft\Validator\Annotation\Mapping\Email;
use Swoft\Validator\Annotation\Mapping\IsInt;
use Swoft\Validator\Annotation\Mapping\IsString;
use Swoft\Validator\Annotation\Mapping\Length;
use App\Annotation\Mapping\PositiveInteger;
use Swoft\Validator\Annotation\Mapping\Max;
use Swoft\Validator\Annotation\Mapping\Min;
use Swoft\Validator\Annotation\Mapping\Url;
use Swoft\Validator\Annotation\Mapping\Validator;

/**
 * 用户登录验证器
 * @Validator(name="ConfigValidator")
 * @package App\Validator
 */
class ConfigValidator
{
    /**
     * @IsString(message="站点名称必须是字符串")
     * @IsRequired(message="站点名称不能为空")
     * @var
     */
    protected $webtitle;

    /**
     * @IsString(message="网址必须是字符串")
     * @IsRequired(message="网址不能为空")
     * @Url(message="必须是一个标准的URL")
     * @var
     */
    protected $weburl;
    /**
     * @IsString(message="邮箱必须是字符串")
     * @IsRequired(message="邮箱不能为空")
     * @Email(message="邮箱格式不正确")
     * @var
     */
    protected $email;
    /**
     * @IsInt(message="QQ格式不正确")
     * @IsRequired(message="QQ不能为空")
     * @Min(value=10000,message="QQ格式不正确")
     * @Max(value=999999999999,message="QQ格式不正确"))
     * @var
     */
    protected $qq;

}
