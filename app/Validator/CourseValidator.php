<?php


namespace App\Validator;

use App\Annotation\Mapping\IsRequired;
use Swoft\Validator\Annotation\Mapping\Alpha;
use Swoft\Validator\Annotation\Mapping\Confirm;
use Swoft\Validator\Annotation\Mapping\IsString;
use Swoft\Validator\Annotation\Mapping\Length;
use App\Annotation\Mapping\PositiveInteger;
use Swoft\Validator\Annotation\Mapping\Validator;

/**
 * 课程验证器
 * @Validator(name="CourseValidator")
 * @package App\Validator
 */
class CourseValidator
{
    /**
     * @IsString(message="标题必须是字符串")
     * @IsRequired(message="标题不能为空")
     * @Length(min=6,max=80,message="标题长度错误")
     * @var
     */
    protected $title;

    /**
     * @IsString(message="图片必须是字符串")
     * @IsRequired(message="图片不能为空")
     * @var
     */
    protected $pic;
}
