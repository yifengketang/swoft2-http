<?php


namespace App\Validator;

use App\Annotation\Mapping\PositiveInteger;
use Swoft\Validator\Annotation\Mapping\File;
use Swoft\Validator\Annotation\Mapping\FileMediaType;
use Swoft\Validator\Annotation\Mapping\FileSize;
use Swoft\Validator\Annotation\Mapping\FileSuffix;
use Swoft\Validator\Annotation\Mapping\Validator;

/**
 * 文件验证器
 * @Validator(name="FileValidator")
 * @package App\Validator
 */
class FileValidator
{
    /**
     * @File(message="请选择上传文件")
     * @FileMediaType(mediaType={"image/gif","image/png","image/jpeg"},message="不支该类型的文件")
     * @FileSize(size=1024000,message="文件过大")
     * @FileSuffix(suffix={"png","jpg","gif"},message="不支该类型的文件")
     * @var
     */
    protected $file;

}
