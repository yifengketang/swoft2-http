<?php


namespace App\Validator;

use App\Annotation\Mapping\IsRequired;
use Swoft\Validator\Annotation\Mapping\Alpha;
use Swoft\Validator\Annotation\Mapping\Confirm;
use Swoft\Validator\Annotation\Mapping\IsString;
use Swoft\Validator\Annotation\Mapping\Length;
use App\Annotation\Mapping\PositiveInteger;
use Swoft\Validator\Annotation\Mapping\Validator;

/**
 * 用户登录验证器
 * @Validator(name="LoginValidator")
 * @package App\Validator
 */
class LoginValidator
{
    /**
     * @IsString(message="用户名必须是字符串")
     * @IsRequired(message="用户不能为空")
     * @Alpha(message="用户名必须是字母组合")
     * @Length(min=3,max=10,message="用户名长度错误")
     * @var
     */
    protected $username;

    /**
     * @IsString(message="密码必须是字符串")
     * @IsRequired(message="密码不能为空")
     * @Length(min=6,max=10,message="密码长度错误")
     * @var
     */
    protected $password;
    /**
     * @IsString(message="旧密码必须是字符串")
     * @IsRequired(message="旧密码不能为空")
     * @Length(min=6,max=10,message="旧密码长度错误")
     * @var
     */
    protected $oldpassword;
    /**
     * @IsString(message="确认密码必须是字符串")
     * @IsRequired(message="确认旧密码不能为空")
     * @Length(min=6,max=10,message="确认旧密码长度错误")
     * @Confirm(name="password",message="两次密码输入不一致")
     * @var
     */
    protected $repassword;

}
