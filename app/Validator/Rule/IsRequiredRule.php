<?php


namespace App\Validator\Rule;


use App\Exception\HttpExecption;
use Swoft\Bean\Annotation\Mapping\Bean;
use Swoft\Validator\Contract\RuleInterface;
use App\Annotation\Mapping\IsRequired;
/**
 * Class IsRequiredRule
 * @Bean(IsRequired::class)
 * @package App\Validator\Rule
 */
class IsRequiredRule implements RuleInterface
{

    /**
     * @inheritDoc
     */
    public function validate(array $data, string $propertyName, $item, $default = null, $strict = false): array
    {
        if(trim($data[$propertyName])==""){
            throw new HttpExecption($item->getMessage(),1);
        }
        return $data;
    }
}
