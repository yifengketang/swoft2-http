<?php


namespace App\Validator\User;

use App\Exception\HttpExecption;
use Swoft\Validator\Annotation\Mapping\Validator;
use Swoft\Validator\Contract\ValidatorInterface;

/**
 * 自定义验证器
 * @Validator(name="NameValidator")
 * @package App\Validator\User
 */
class UserNameValidator implements ValidatorInterface
{
    /**
     * @param array $data
     * @param array $params
     * @return array
     * @throws HttpExecption
     */
    public function validate(array $data, array $params): array
    {
        // TODO: Implement validate() method.
        if($data['username']=="admin" || $data['username']=="yifeng"){
            return $data;
        }
        throw new HttpExecption("账号无权限");
    }

}
