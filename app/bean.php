<?php

use Swoft\Http\Server\HttpServer;

return [
    'logger'     => [
        'flushRequest' => false,
        'enable'       => false,
        'json'         => false,
    ],
    'httpServer' => [
        'class'   => HttpServer::class,
        'port'    => 18306,
        /* @see HttpServer::$setting */
        'setting' => [
            'worker_num' => 8,
            // enable static handle
            'enable_static_handler'    => true,
            // swoole v4.4.0以下版本, 此处必须为绝对路径
            'document_root'            => dirname(__DIR__) . '/public',
        ]
    ],
    'db'=>[
        'class'    => Swoft\Db\Database::class,
        'dsn'      => 'mysql:dbname=yf_database;host=192.168.3.138:23306',
        'username' => 'root',
        'password' => '123456',
        'charset'  => 'utf8mb4',
        'prefix'   => 'yf_',
    ],
    'redis'      => [
        'class'    => \Swoft\Redis\RedisDb::class,
        'driver'   => 'phpredis',
        'host'     => '192.168.3.138',
        'port'     => 16379,
        'database' => 0,
    ],
    'httpDispatcher'    => [
        'middlewares'      => [
            \Swoft\Http\Session\SessionMiddleware::class,
        ],
        // ......
        'afterMiddlewares' => [
            \Swoft\Http\Server\Middleware\ValidatorMiddleware::class
        ]
    ],
    'sessionHandler' => [
        'class'    => \Swoft\Http\Session\Handler\RedisHandler::class,
        // set redis pool
        'redis' => bean('redis.pool'),
        'expireTime'=>1800
    ],
];
