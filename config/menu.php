<?php
return [
    "clearInfo"=> [
    "clearUrl"=> "api/clear.json"
  ],
  "homeInfo"=> [
    "title"=> "首页",
    "icon"=> "fa fa-home",
    "href"=> "/admin/welcome"
  ],
  "logoInfo"=> [
    "title"=> "Swoft2.x",
    "image"=> "/admin/images/logo.png",
    "href"=> ""
  ],
  "menuInfo"=> [
    "currency"=> [
        "title"=> "系统管理",
      "icon"=> "fa fa-address-book",
      "child"=> [
        [
            "title"=> "站点配置",
          "href"=> "/admin/config",
          "icon"=> "fa fa-window-maximize",
          "target"=> "_self"
        ],
      [
          "title"=> "SEO配置",
          "href"=> "/admin/config",
          "icon"=> "fa fa-window-maximize",
          "target"=> "_self"
      ]
      ]
    ],
    "component"=> [
        "title"=> "内容管理",
      "icon"=> "fa fa-lemon-o",
      "child"=> [
        [
            "title"=> "课程管理",
          "href"=> "",
          "icon"=> "fa fa-meetup",
          "target"=> "",
          "child"=> [
            [
                "title"=> "课程列表",
              "href"=> "/admin/course/list",
              "icon"=> "fa fa-calendar",
              "target"=> "_self"
            ],
            [
                "title"=> "添加课程",
              "href"=> "/admin/course/create",
              "icon"=> "fa fa-calendar",
              "target"=> "_self"
            ]
          ]
        ]
      ]
    ],
    "other"=> [
        "title"=> "系统安全",
      "icon"=> "fa fa-slideshare",
      "child"=> [
        [
            "title"=> "密码修改",
          "href"=> "/admin/setpassword",
          "icon"=> "fa fa-superpowers",
          "target"=> "_self"
        ]
      ]
    ]
  ]
];
