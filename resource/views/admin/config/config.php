<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>站点配置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/admin/lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="/admin/css/public.css" media="all">
    <style>
        .layui-form-item .layui-input-company {width: auto;padding-right: 10px;line-height: 38px;}
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">
        <div class="layui-form layuimini-form">
            <div class="layui-form-item">
                <label class="layui-form-label required">网站名称</label>
                <div class="layui-input-block">
                    <input type="text" name="webtitle" lay-verify="required" lay-reqtext="网站域名不能为空" placeholder="请输入网站名称"  value="<?=$config['webtitle']??''?>" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label required">域名</label>
                <div class="layui-input-block">
                    <input type="text" name="weburl" lay-verify="required" lay-reqtext="网址" placeholder="请输入网址"  value="<?=$config['weburl']??''?>" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label required">Email</label>
                <div class="layui-input-block">
                    <input type="text" name="email" lay-verify="required" lay-reqtext="邮箱" placeholder="请输入邮箱"  value="<?=$config['email']??''?>" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label required">QQ</label>
                <div class="layui-input-block">
                    <input type="text" name="qq" lay-verify="required" lay-reqtext="QQ" placeholder="请输入网站QQ"  value="<?=$config['qq']??''?>" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">META关键词</label>
                <div class="layui-input-block">
                    <textarea name="webkey" class="layui-textarea" placeholder="多个关键词用英文状态 , 号分割"><?=$config['webkey']??''?></textarea>
                </div>
            </div>
            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">META描述</label>
                <div class="layui-input-block">
                    <textarea name="webdesc" class="layui-textarea"><?=$config['webdesc']??''?></textarea>
                </div>
            </div>
            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label required">版权信息</label>
                <div class="layui-input-block">
                    <textarea name="copyright" class="layui-textarea"><?=$config['copyright']??''?></textarea>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="setting">确认保存</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/admin/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form',"jquery"], function () {
        var form = layui.form
            , layer = layui.layer,
            $ = layui.$;

        //监听提交
        form.on('submit(setting)', function (data) {
            $.ajax({
                type:"post",
                url:"/admin/saveconfig",
                data:data.field,
                success:function(res){
                    layer.msg(res.msg, {},function () {
                        if(res.code==3){
                            location.href = "/admin/login";
                        }
                    });
                },
                error:function(res){
                    layer.msg(res.responseJSON.msg);
                }
            });

            return false;
        });

    });
</script>
</body>
</html>
