<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>添加课程</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/admin/lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="/admin/css/public.css" media="all">
    <style>
        .layui-form-item .layui-input-company {width: auto;padding-right: 10px;line-height: 38px;}
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">
        <form class="layui-form layuimini-form" id="myform">
            <div class="layui-form-item">
                <label class="layui-form-label required">标题</label>
                <div class="layui-input-block">
                    <input type="text" name="title" lay-verify="required" lay-reqtext="标题不能为空" placeholder="请输入标题"  value="<?=$config['webtitle']??''?>" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">META关键词</label>
                <div class="layui-input-block">
                    <textarea name="keyword" class="layui-textarea" placeholder="多个关键词用英文状态 , 号分割"><?=$config['webkey']??''?></textarea>
                </div>
            </div>
            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">META描述</label>
                <div class="layui-input-block">
                    <textarea name="desc" class="layui-textarea"><?=$config['webdesc']??''?></textarea>
                </div>
            </div>
            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label required">摘要</label>
                <div class="layui-input-block">
                    <textarea name="digest" class="layui-textarea"><?=$config['copyright']??''?></textarea>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label required">课程图片</label>
                <div class="layui-input-block">
                    <input type="text" name="pic" id="pic" lay-verify="required" lay-reqtext="课程图片不能为空" placeholder="请输入课程图片" class="layui-input">

                    <button type="button" class="layui-btn" id="uploadimg">
                        <i class="layui-icon">&#xe67c;</i>上传图片
                    </button>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label required">是否推荐</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="isshow" title="推荐" value="1" lay-skin="primary">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label required">详情</label>
                <div class="layui-input-block">
                    <div id="editor" style="margin: 50px 0 50px 0">

                    </div>
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="setting">确认保存</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="/admin/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script src="/admin/js/lay-config.js?v=1.0.4" charset="utf-8"></script>
<script>
    layui.use(['form',"jquery","upload",'wangEditor'], function () {
        var form = layui.form
            , layer = layui.layer,
            $ = layui.$,
            upload = layui.upload,
            wangEditor = layui.wangEditor;
        //图片上传
        var uploadInst = upload.render({
            elem: '#uploadimg' //绑定元素
            ,url: '/admin/upload' //上传接口
            ,done: function(res){
                //上传完毕回调
                layer.msg(res.msg,{},function(){
                    if(res.code==0){
                        $("#pic").val(res.data.img);
                    }
                });
            }
        });
        //编辑器
        var editor = new wangEditor('#editor');
        editor.customConfig.uploadImgServer = "/admin/upload";
        editor.customConfig.uploadFileName = 'file';
        editor.customConfig.pasteFilterStyle = false;
        editor.customConfig.uploadImgMaxLength = 5;
        editor.customConfig.uploadImgHooks = {
            // 上传超时
            timeout: function (xhr, editor) {
                layer.msg('上传超时！')
            },
            // 如果服务器端返回的不是 {errno:0, data: [...]} 这种格式，可使用该配置
            customInsert: function (insertImg, result, editor) {
                if (result.code == 0) {
                    var url = result.data.img;
                    insertImg(url);
                    // url.forEach(function (e) {
                    //     insertImg(e);
                    // })
                } else {
                    layer.msg(result.msg);
                }
            }
        };
        editor.customConfig.customAlert = function (info) {
            layer.msg(info);
        };
        editor.create();
        //监听提交
        form.on('submit(setting)', function (data) {
            var datas = data.field;
            datas.content = editor.txt.html();
            $.ajax({
                type:"post",
                url:"/admin/course/save",
                data:datas,
                success:function(res){
                    layer.msg(res.msg, {},function () {
                        //重置表单
                        $("#myform")[0].reset();
                        editor.txt.clear()
                        layui.form.render();
                        if(res.code==3){
                            location.href = "/admin/login";
                        }
                    });
                },
                error:function(res){
                    layer.msg(res.responseJSON.msg);
                }
            });

            return false;
        });

    });
</script>
</body>
</html>
