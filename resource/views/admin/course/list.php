<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>课程列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/admin/lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="/admin/css/public.css" media="all">
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">

        <fieldset class="table-search-fieldset">
            <legend>搜索信息</legend>
            <div style="margin: 10px 10px 10px 10px">
                <form class="layui-form layui-form-pane" action="">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">课程标题</label>
                            <div class="layui-input-inline">
                                <input type="text" name="title" autocomplete="off" class="layui-input">
                            </div>
                        </div>

                        <div class="layui-inline">
                            <button type="submit" class="layui-btn layui-btn-primary" lay-submit  lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                        </div>
                    </div>
                </form>
            </div>
        </fieldset>

        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-sm data-add-btn"> 添加课程 </button>
                <button class="layui-btn layui-btn-sm layui-btn-danger data-delete-btn"> 删除课程 </button>
            </div>
        </script>

        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>

        <script type="text/html" id="currentTableBar">
            <a class="layui-btn layui-btn-xs data-count-edit" lay-event="edit">编辑</a>
            <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a>
        </script>
        <!--图片-->
        <script type="text/html" id="imgTpl">
            <img src="{{d.pic}}" alt="" height="50">
        </script>
        <!--推荐-->
        <script type="text/html" id="showTpl">
            <input type="checkbox" name="isshow" value="{{d.id}}" lay-skin="switch" lay-text="推荐|取消" lay-filter="isshow" {{ d.isshow == 1 ? 'checked' : '' }}>
        </script>
    </div>
</div>
<script src="/admin/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'table'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            layuimini = layui.layuimini;

        table.render({
            elem: '#currentTableId',
            url: '/admin/course/getdata',
            toolbar: '#toolbarDemo',
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                icon: 'layui-icon-tips'
            }],
            cols: [[
                {type: "checkbox", width: 50, fixed: "left"},
                {field: 'id', width: 80, title: 'ID', sort: true},
                {field: 'title', title: '标题'},
                {field: 'pic', width: 135, title: '图片',templet:"#imgTpl"},
                {field: 'isshow', width: 110, title: '是否推荐',templet:"#showTpl"},
                {title: '操作', width: 180, templet: '#currentTableBar', fixed: "right", align: "center"}
            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 15,
            page: true
        });

        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            // var result = JSON.stringify(data.field);
            var result = data.field;
            //执行搜索重载
            table.reload('currentTableId', {
                page: {
                    curr: 1
                }
                , where: {
                    title:result.title
                }
            }, 'data');

            return false;
        });

        // 监听添加操作
        $(".data-add-btn").on("click", function () {

            var index = layer.open({
                title: '添加课程',
                type: 2,
                shade: 0.2,
                maxmin:true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/course/create',
            });
            $(window).on("resize", function () {
                layer.full(index);
            });

            return false;
        });

        // 监听删除操作
        $(".data-delete-btn").on("click", function () {
            var checkStatus = table.checkStatus('currentTableId')
                , data = checkStatus.data;
            var arr = new Array();
            for (let i = 0; i < data.length; i++) {
                arr.push(data[i].id);
            }
            var ids = arr.join(",");
            $.ajax({
                type:"post",
                url:"/admin/course/del",
                data:{ids:ids},
                success:function(res){
                    layer.msg(res.msg,{},function(){
                        if(res.code==0){
                            table.reload('currentTableId');
                        }
                    });
                }
            });
        });

        form.on('switch(isshow)', function(data){
            var isshow =0;
            if(data.elem.checked){
                isshow =1;
            }
            $.ajax({
                type:"post",
                url:"/admin/course/setshow",
                data:{id:data.value,isshow:isshow},
                success:function(res){
                    layer.msg(res.msg,{});
                }
            });
        });

        table.on('tool(currentTableFilter)', function (obj) {
            var data = obj.data;
            if (obj.event === 'edit') {

                var index = layer.open({
                    title: '编辑课程',
                    type: 2,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: ['100%', '100%'],
                    content: '/admin/course/edit/'+data.id,
                    end:function(){
                        table.reload('currentTableId');
                    }
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
                return false;
            } else if (obj.event === 'delete') {
                layer.confirm('真的删除行?', function (index) {
                    $.ajax({
                        type:"post",
                        url:"/admin/course/del",
                        data:{ids:data.id},
                        success:function(res){
                            layer.msg(res.msg,{},function(){
                                if(res.code==0){
                                    obj.del();
                                }
                            });
                        }
                    });
                    layer.close(index);
                });
            }
        });

    });
</script>
<script>

</script>

</body>
</html>
