<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=get_config("webtitle")?></title>
    <meta name="keywords" content="<?=get_config("webkey")?>">
    <meta name="description" content="<?=get_config("webdesc")?>">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="/home/css/style.css">
</head>
<body>
<?php $this->include('home/common/header') ?>
    <div class="main index">
        <div class="title text-center mt-5 mb-5"><h3>最新课程</h3></div>
        <div class="container list">
            <div class="row items">
              <?foreach ($list as $l):?>
                <!-- start list -->
                <div class="col-6 col-sm-6 col-md-4 col-lg-3 mb-4 item">
                    <div class="box">
                        <div class="img"><img src="<?=$l['pic']?>" class="img-fluid" alt="" srcset=""></div>
                        <div class="mt-2 title"><?=$l['title']?></div>
                        <div class="desc">
                          <?=$l['digest']?>
                        </div>
                    </div>
                </div>
                <!-- end list -->
              <?endforeach?>
            </div>
        </div>
    </div>
    <!-- start footer -->
    <?php $this->include('home/common/footer') ?>
    <!-- end footer -->
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
