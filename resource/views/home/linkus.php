<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=get_config("webtitle")?>-联系我们</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="/home/css/style.css">
</head>
<body>
    <?php $this->include('home/common/header') ?>
    <div class="main news">
      <div class="container title mt-5 mb-5 border-bottom"><h3>联系我们</h3></div>
        <div class="container page">
            <h3>易风课堂</h3>
            <p>
                网址：http://www.yifengkt.cn
            </p>
            <p>
                QQ：576617109
            </p>
            <p>
                PHP交流群：494826865
            </p>
        </div>
    </div>
    <!-- start footer -->
    <?php $this->include('home/common/footer') ?>
    <!-- end footer -->
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
