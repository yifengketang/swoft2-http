<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=get_config("webtitle")?>-课程列表</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="/home/css/style.css">
</head>
<body>
<?php $this->include('home/common/header') ?>
    <div class="main news">
      <div class="container title mt-5 mb-5"><h3>最新课程</h3></div>
        <div class="container list">
            <div class="row items">

            </div>
        </div>
        <div class="text-center">
          <button class="btn btn-danger mt-3 mb-3" id="loaddata">加载更多</button>
        </div>
    </div>
    <!-- start footer -->
    <?php $this->include('home/common/footer') ?>
    <!-- end footer -->
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
  function createItem(pic='',title='',desc=''){
    return `<div class="col-6 col-sm-6 col-md-4 col-lg-3 mb-4 item">
                    <div class="box">
                        <div class="img"><img src="${pic}" class="img-fluid" alt="" srcset=""></div>
                        <div class="mt-2 title">${title}</div>
                        <div class="desc">
                          ${desc}
                        </div>
                    </div>
                </div>`;
  }

  // $("#loaddata").on('click',function(){
  //   var myRequest = new Request('./js/data.json');
  //   fetch('./js/data.json',{method:"GET"})
  //   .then(response => response.json())
  //   .then(data=>{
  //     console.log(data);
  //     var str="";
  //     data.list.forEach((value, index, array) => {
  //       str = createItem(value.pic,value.title,value.desc);
  //         $(".items").append(str);
  //       })
  //   });
  // });
  //分页处理
  var page =0;
  var limit =10;
  var count =5;
  req(++page,limit);
  $("#loaddata").on('click',function(){
    if(page > count-1){
      alert("没有数据了");
      return;
    }
    req(++page,limit);
  });
  function req(page,limit){
    $.ajax({
      url:'/api/getcourses',
      type:'get',
      data:{page:page,limit:limit},
      success:function(res){
        if(page==1){
          count = Math.ceil(res.count/limit);
        }
        for(var i=0;res.data.length;i++){
          str = createItem(res.data[i].pic,res.data[i].title,res.data[i].desc);
          $(".items").append(str);
        }
      }
    })
  }
</script>
</body>
</html>
